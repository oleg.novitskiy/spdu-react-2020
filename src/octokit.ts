import {Octokit} from '@octokit/rest';

const auth = process.env.REACT_APP_AUTH_TOKEN;

const octokit = new Octokit({
    auth
});

export {octokit};
