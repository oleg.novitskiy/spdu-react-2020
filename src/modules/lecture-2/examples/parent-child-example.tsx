import React, {Component} from "react";

type State = {
    modified: boolean
};

export class Parent extends Component<{}, State> {
    state = {
        modified: false
    };

    onChange = () => {
        this.setState({modified: true});
    };

    onClear = () => {
        this.setState({modified: false});
    };

    render() {
        return (
            <div className="parent">
                Parent.
                {this.state.modified ?
                    <div onClick={this.onClear}>I'm modified by child. Click here to clear state</div>
                    : null
                }
                <Child onChange={this.onChange}/>
            </div>
        )
    }
}

// New file

type  Props = {
    onChange: () => void
}

const Child = (props: Props) => {
    return <div className="child" onClick={props.onChange}>Child. Click me</div>
};
