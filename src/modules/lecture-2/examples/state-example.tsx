import React, {Component} from 'react';
import clsx from "clsx";

type Props = {
    type: 'PRIMARY' | 'SECONDARY',
    caption: string,
    onClick: () => void
}

export class StateExample extends Component<Props> {
    static defaultProps = {
        onClick: () => {}
    };

    state = {
        busy: false
    };

    onClick = () => {
        this.setState({busy: true});
        this.props.onClick();
    };


    render() {
        const classes = clsx('button', {
            'button_primary': this.props.type === 'PRIMARY',
            'button_secondary': this.props.type === 'SECONDARY'
        });

        return (
            <button className={classes} onClick={this.onClick} disabled={this.state.busy}>
                {this.props.caption}
                {this.state.busy && <span>...</span>}
            </button>
        );
    }
}
