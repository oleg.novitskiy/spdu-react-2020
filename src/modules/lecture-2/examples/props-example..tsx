import React, {Component} from 'react';

interface Props {
    data: string[]
}

export class PropsExample extends Component<Props, any> {
    render() {
        if (!this.props.data.length) {
            return <div>No data to display</div>;
        }

        return (
            <>
                {this.props.data.map(item => <span>{item}</span>)}
            </>
        )
    }
}


