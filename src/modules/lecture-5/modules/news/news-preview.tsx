import React, {FC} from 'react';
import {NewsItem} from './typedef';
import './news.css';

type Props = {
    data: NewsItem
};

export const NewsPreview: FC<Props> = ({data}) => {
    const date = new Date(data.publishedAt);
    const day = new Intl.DateTimeFormat('en-US', {day: '2-digit'}).format(date);
    const month = new Intl.DateTimeFormat('en-US', {month: 'short'}).format(date);

    return (
        <div className="post-module">

            <div className="thumbnail">
                <div className="date">
                    <div className="day">{day}</div>
                    <div className="month">{month}</div>
                </div>
                <img src={data.urlToImage} alt="preview"/>
            </div>

            <div className="post-content">
                <h1 className="title">{data.title}</h1>
                <h2 className="sub_title">{data.description}</h2>
                <p className="description">{data.description}</p>
                <div className="post-meta"><span className="timestamp"><i
                    className="fa fa-clock-">o</i> 6 mins ago</span></div>
            </div>
        </div>);
};
