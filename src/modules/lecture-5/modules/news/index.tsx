import React, {ChangeEvent, FC, useEffect, useState} from 'react';
import {NewsPreview} from './news-preview';
import {NewsItem} from './typedef';
import {useQuery} from '../../../lecture-7/hooks/use-query';

export const News: FC<{}> = () => {
    const {query: browserQuery, setQuery: setBrowserQuery} = useQuery();
    const [news, setNews] = useState<NewsItem[]>([]);
    const [query, setQuery] = useState<string>(browserQuery.get('tag') || '');

    useEffect(() => {
        if (!query) {
            return;
        }
        fetch(`https://newsapi.org/v2/everything?q=${query}&sortBy=publishedAt&pageSize=30&apiKey=1c4baa95f2f14850b0bf0e2c74066adb`)
            .then(response => response.json())
            .then(data => setNews(data.articles));
    }, [query]);

    const onBlur = () => {
        setBrowserQuery('tag', query);
    }

    const onInputChange = (e: ChangeEvent<HTMLInputElement>) => {
        setQuery(e.target.value)
    }

    return (
        <div className="news">
            <h2>News</h2>
            <input type="text" onChange={onInputChange} onBlur={onBlur} value={query}/>
            {
                news.length
                    ? news.map((newsItem, idx) => {
                        return <NewsPreview data={newsItem} key={idx}/>
                    })
                    : <div>There are no articles for {query} tag</div>
            }
        </div>
    )
};

export default News;
