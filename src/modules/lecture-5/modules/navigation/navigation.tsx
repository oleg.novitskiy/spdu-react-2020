import React, {FC} from 'react';
import {Nav, Navbar, NavbarLink, Item as NavbarItem} from '../../components/navbar';
import {FormInline} from "../../components/form-inline";
import {UserInfo} from '../../../lecture-3/context-example/modules/user-info';


type Props = {};

export const Navigation: FC<Props> = () => {
    return (
        <Navbar>
            <>
                <Nav>
                    <NavbarItem>
                        <NavbarLink url="/lecture-5/home" caption="Home"/>
                    </NavbarItem>
                    <NavbarItem>
                        <NavbarLink url="/lecture-5/about" caption="About"/>
                    </NavbarItem>
                    <NavbarItem>
                        <NavbarLink url="/lecture-5/news" caption="News"/>
                    </NavbarItem>
                    <NavbarItem>
                        <NavbarLink url="/lecture-5/users" caption="Users"/>
                    </NavbarItem>
                </Nav>
                <FormInline>
                    <UserInfo/>
                </FormInline>
            </>
        </Navbar>
    )
};
