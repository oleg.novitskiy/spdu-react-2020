import React, {FC, useEffect, useState} from 'react';
import {NavLink, useLocation} from 'react-router-dom';
import {octokit} from '../../../../octokit';
import {components} from '@octokit/openapi-types'
import {Loader} from '../../../../components/loader/loader';

type Contributor = components["schemas"]["contributor"];

export const Users: FC = () => {
    const [users, setUsers] = useState<Contributor[]>();
    const queryParams = new URLSearchParams(useLocation().search);
    const pageSize = queryParams.get('pageSize');

    useEffect(() => {


        octokit.repos.listContributors({
            owner: 'facebook',
            repo: 'react',
            per_page: pageSize !== null ? +pageSize : 15
        }).then(collaborators => {
            if (collaborators.status === 200) {
                setUsers(
                    collaborators.data.sort((a, b) => a.contributions - b.contributions)
                );
            }
        })
    }, [pageSize]);

    if (!users) {
        return <Loader/>;
    }

    return (
        <div>
            <h2>React contributors</h2>
            <ul>
                {
                    users.map(user => (
                        <li key={user.id}>
                            <NavLink to={`/lecture-5/users/${user.login}`}>{user.login}</NavLink>
                        </li>
                    ))
                }
            </ul>
        </div>
    )
};
