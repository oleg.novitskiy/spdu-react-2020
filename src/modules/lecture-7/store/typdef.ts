import {rootReducer} from './root-reducer';
import {store} from './store';

export type AppState = ReturnType<typeof rootReducer>;
export type AppDispatch = typeof store.dispatch;


declare module 'react-redux' {
    export interface DefaultRootState extends AppState {
    }
}


// type Store = {
//     issues: {
//         data: Issue[],
//         loading: boolean,
//         error: string | null
//     },
//     comments: {
//         [org: string]: {
//             [repo: string]: {
//                  [issueNumber: number]: IssueComment[]
//             }
//         }
//     }
// }
