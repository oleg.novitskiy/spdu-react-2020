import {Middleware} from 'redux';
// import {unwrapResult} from '@reduxjs/toolkit'

export const handleErrors: Middleware = () => next => action => {
    if (action.error) {
        if (action.error.message === 'Bad credentials') {
            window.location.replace('/auth');
        }
    }
    next(action);
}
