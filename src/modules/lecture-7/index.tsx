import {Redirect, Route, Switch, useRouteMatch} from 'react-router-dom';
import {Repos} from "./modules/repos-search/repos";
import {RepoDetails} from "./modules/repo-details/repo-details";
import {IssuesList} from "./modules/issues/issues";
import {IssueDetails} from "./modules/issues/issue-details";


export const Lecture7Examples = () => {
    let {path} = useRouteMatch();

    return (
        <Switch>
            <Route path={`${path}/`} component={Repos} exact/>
            <Route path={`${path}/:owner/:repo`} component={RepoDetails} exact/>
            <Route path={`${path}/:owner/:repo/issues`} component={IssuesList} exact/>
            <Route path={`${path}/:owner/:repo/issues/:id`} component={IssueDetails}/>
            <Redirect to={`${path}/`}/>
        </Switch>
    );
}
