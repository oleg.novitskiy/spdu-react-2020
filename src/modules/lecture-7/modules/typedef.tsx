import {components} from '@octokit/openapi-types/generated/types';

export type Issue = components['schemas']['issue'];
export type IssueComment = components['schemas']['issue-comment'];
export type RepoSearchResult = components['schemas']['repo-search-result-item'];
export type Repository = components['schemas']['full-repository'];


export type OwnerRepo = {
    owner: string,
    repo: string
}