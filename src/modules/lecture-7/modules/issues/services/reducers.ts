import {createReducer} from '@reduxjs/toolkit';
import {Issue} from '../../typedef';
import {loadIssuesReject, loadIssuesRequest, loadIssuesSuccess} from './actions';

type State = {
    data: Issue[],
    loading: boolean,
    error: string | null
};

const defaultState = {
    data: [],
    loading: false,
    error: null
}

export const issues = createReducer<State>(defaultState, builder =>
    builder
        .addCase(loadIssuesRequest, state => ({
            ...state,
            loading: true
        }))
        .addCase(loadIssuesSuccess, (state, action) => ({
            ...state,
            data: action.payload,
            loading: false
        }))
        .addCase(loadIssuesReject, state => ({
            ...state,
            error: 'Something went wrong'
        }))
        .addDefaultCase(state => state)
)
