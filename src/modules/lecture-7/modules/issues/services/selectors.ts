import {AppState} from '../../../store/typdef';

export const getIssues = (state: AppState) => state.issues.data;