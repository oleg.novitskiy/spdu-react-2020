import {createAction} from '@reduxjs/toolkit';
import {octokit} from '../../../../../octokit';
import {AppDispatch} from '../../../store/typdef';
import {Issue, OwnerRepo} from '../../typedef';


// export const loadIssues = createAsyncThunk('issues/get', async () => {
//     return (await octokit.issues.listForAuthenticatedUser()).data
// })

export const loadIssuesRequest = createAction('issues/get/request');
export const loadIssuesSuccess = createAction<Issue[]>('issues/get/success');
export const loadIssuesReject = createAction('issues/get/reject');


export const loadIssues = ({owner, repo}: OwnerRepo) => async (dispatch: AppDispatch) => {
    dispatch(loadIssuesRequest());

    try {
        const response = await octokit.issues.listForRepo({
            owner,
            repo
        });
        if (response.status === 200) {
            dispatch(loadIssuesSuccess(response.data));
        }
    }
    catch (e) {
        dispatch(loadIssuesReject());
    }
}