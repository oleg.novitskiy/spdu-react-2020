import {useParams} from 'react-router-dom';
import {useEffect, useState} from 'react';
import {octokit} from '../../../../octokit';
import {Issue, OwnerRepo} from '../typedef';
import {Avatar, Card, CardContent, CardHeader, Container} from '@material-ui/core';
import {Comments} from './comments/comments';

interface Props {

}

interface RouterParams extends OwnerRepo {
    id: string
}

export const IssueDetails = () => {
    const {id, owner, repo} = useParams<RouterParams>();
    const [issue, setIssue] = useState<Issue>();

    useEffect(() => {
        octokit.issues.get({issue_number: +id, owner, repo})
            .then(issues => {
                if (issues.status === 200) {
                    setIssue(issues.data);
                }
            });
    }, []);


    return (
        <Card>
            <CardHeader
                avatar={<Avatar src={issue?.user?.avatar_url} />}
                title={issue?.title}
                subheader={issue?.created_at}
            />
            <CardContent>
                {issue?.body}
                {issue && <Comments/>}
            </CardContent>
        </Card>
    );
};
