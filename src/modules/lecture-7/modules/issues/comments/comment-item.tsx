import React, {FC} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import {IssueComment} from '../../typedef';
import {Avatar, CardHeader} from '@material-ui/core';

const useStyles = makeStyles({
    root: {
        minWidth: 275,
        marginTop: 25
    },
    title: {
        fontSize: 14,
    },
    pos: {
        marginBottom: 12,
    },
});

interface Props {
    comment: IssueComment,
    removeComment: (id: number) => void
}

export const CommentItem: FC<Props> = ({comment, removeComment}) => {
    const classes = useStyles();

    const onRemoveComment = () => removeComment(comment.id);

    return (
        <Card className={classes.root}>
            <CardHeader
                avatar={<Avatar src={comment.user?.avatar_url}/>}
                title={comment.user?.login}
                subheader={comment.created_at}
            />
            <CardContent>
                <Typography variant="body2" color="textPrimary">
                    {comment.body}
                </Typography>
            </CardContent>
            <CardActions>
                <Button size="small">Reply</Button>
                {
                    // @ts-ignore author_association described in lowercase
                    comment.author_association === 'OWNER' && <Button size="small" onClick={onRemoveComment}>Remove comment</Button>
                }
            </CardActions>
        </Card>
    );
}