import {useEffect} from 'react';
import IssuePreview from './issue-preview';
import {List} from '@material-ui/core';
import {makeStyles} from '@material-ui/core/styles';
import {useSelector, useDispatch} from 'react-redux';
import {loadIssues} from './services/actions';
import {getIssues} from './services/selectors';
import {useParams} from "react-router-dom";
import {OwnerRepo} from "../typedef";

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
        backgroundColor: theme.palette.background.paper,
    },
}));




export const IssuesList = () => {
    const {owner, repo} = useParams<OwnerRepo>();
    const styles = useStyles();
    const dispatch = useDispatch();
    const issues = useSelector(getIssues);

    useEffect(() => {
        dispatch(loadIssues({owner, repo}));
    }, []);


    return (
        <div className={styles.root}>
            <List>
                {
                    issues.map((issue, idx) => {
                    return <IssuePreview issue={issue} key={issue.id} divider={idx !== 0}/>;
                })}
            </List>
        </div>
    )
}

