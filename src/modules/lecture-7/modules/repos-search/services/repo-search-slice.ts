import {createAsyncThunk, createEntityAdapter, createSlice} from '@reduxjs/toolkit';
import {octokit} from '../../../../../octokit';
import {RepoSearchResult} from '../../typedef';
import {AppState} from '../../../store/typdef';

const reposAdapter = createEntityAdapter<RepoSearchResult>();

export const loadRepos = createAsyncThunk('repos', async (q: string) => {
    return (await octokit.search.repos({q})).data.items
});

export const repoSearch = createSlice({
    name: 'repos',
    initialState: reposAdapter.getInitialState({
            loading: false
        }
    ),
    reducers: {
        clearData: reposAdapter.removeAll
    },
    extraReducers: builder => builder
        .addCase(loadRepos.fulfilled, (state, action) => {
            reposAdapter.setAll(state, action);
            state.loading = false;
        })
        .addCase(loadRepos.pending, state => {
            state.loading = true;
        })
        .addCase(loadRepos.rejected, state => {
            state.loading = false;
        })
});

export const repoSearchSelectors = reposAdapter.getSelectors<AppState>(state => state.reposSearch);

export const {clearData} = repoSearch.actions;