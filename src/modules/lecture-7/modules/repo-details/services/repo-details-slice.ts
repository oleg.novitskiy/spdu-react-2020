import {createAsyncThunk, createEntityAdapter, createSlice} from '@reduxjs/toolkit';
import {Repository} from '../../typedef';
import {octokit} from '../../../../../octokit';
import {AppState} from '../../../store/typdef';
import {makeOwnerRepoId} from '../../repos-search/utils/make-owner-repo-Id';

const reposAdapter = createEntityAdapter<Repository>({
    selectId: repo => makeOwnerRepoId({
        owner: repo.owner?.login,
        repo: repo.name
    })
});

interface LoadRepoParams {
    owner: string,
    repo: string
}

export const loadRepoInfo = createAsyncThunk('repo', async ({owner, repo}: LoadRepoParams) => {
    return (await octokit.repos.get({owner, repo})).data;
})

export const repoDetails = createSlice({
    name: 'repos',
    initialState: reposAdapter.getInitialState(),
    reducers: {},
    extraReducers: builder => builder
        .addCase(loadRepoInfo.fulfilled, reposAdapter.addOne)
})

export const repoSelectors = reposAdapter.getSelectors<AppState>(state => state.repoDetails);