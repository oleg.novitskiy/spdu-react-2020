import React, {FC} from 'react';
import {Users} from './users';
import {NavLink} from 'react-router-dom';
import {Contributor} from './typedef';

type Props = {};

export const UsersGrid: FC<Props> = () => {
    return (
        <Users>{
            users => (
                <div className="row">
                    {
                        users.map((user: Contributor) => (
                            <div className="col-lg-3" key={user.id}>
                                <img src={user.avatar_url} width={48} alt={user.name}/>
                                <NavLink to={`/lecture-6/users/${user.login}`}>{user.login}</NavLink>
                            </div>
                        ))
                    }
                </div>
            )
        }
        </Users>
    )
};
