import {withPagination} from '../withPagination';

import React from 'react';
import {Contributor} from './typedef';

type PaginatedProps = { pageData: Contributor[] };

const PaginatedView = (props: PaginatedProps) => {
    return (
        <div>
            {props.pageData.map(user => (
                <div key={user.id}>{user.login}</div>
            ))}
        </div>
    )
};

export default withPagination<Contributor>(PaginatedView);
