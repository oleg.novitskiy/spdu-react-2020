import React, {FC} from 'react';
import {Users} from './users';
import {NavLink} from 'react-router-dom';
import {Contributor} from './typedef';


export const UsersList: FC<{}> = () => {
    return (
        <Users>{
            (users) => (
                <ul>{
                    users.map((user: Contributor) => (
                        <li key={user.id}>
                            <img src={user.avatar_url} width={48} alt={user.name}/>
                            <NavLink to={`/users/${user.login}`}>{user.login}</NavLink>
                        </li>
                    ))
                }
                </ul>
            )}
        </Users>
    )
};
