import {Users} from './users';
import PaginatedView from './users-with-pagination';

const UsersView = () => (
    <Users>
        {users => <PaginatedView data={users}/>}
    </Users>
);

export default UsersView;