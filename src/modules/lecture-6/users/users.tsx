import React, {FC, ReactNode, useEffect, useState} from 'react';
import {Contributor} from './typedef';
import {octokit} from '../../../octokit';

type Props = {
    children: (users: Contributor[]) => ReactNode
}
export const Users: FC<Props> = ({children}) => {
    const [users, setUsers] = useState<Contributor[] | null>(null);
    useEffect(() => {

        octokit.repos.listContributors({
            owner: 'facebook',
            repo: 'react',
            per_page: 150
        }).then(collaborators => {
            if (collaborators.status === 200) {
                const data = collaborators.data
                    .sort((a, b) => a.contributions - b.contributions);
                setUsers(data);
            }
        })
    }, []);

    if (!users) {
        return null;
    }
    return (
        <div>
            <h2>React contributors</h2>
            {children(users)}
        </div>
    )
};
