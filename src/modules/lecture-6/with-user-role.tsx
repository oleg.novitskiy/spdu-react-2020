import React, {ComponentType, useEffect, useState} from 'react';

export interface WithUserRoleProps {
    userRole: 'admin' | 'user' | 'unauthorized'
}
export function withUserRole<T extends WithUserRoleProps>(WrappedComponent: ComponentType<T>) {
    type Props = Omit<T, 'userRole'>

    return (props: Props) => {
        const [userRole, setUserRole] = useState('unauthorized');

        useEffect(() => {
            setUserRole(fakeServer.getUserRole());
        }, []);

        return <WrappedComponent userRole={userRole} {...props as any}/>
    }
}


const fakeServer = {
    getUserRole: () => {
        return 'admin'
    }
};
