import {useMemo} from 'react';

type Predicate<T> = (item: T) => boolean;

export function useFilter<T>(data: T[], predicates: Predicate<T>[]) {
    return useMemo(
        () => predicates.reduce((memo, predicate) => memo.filter(predicate), data),
        [data, predicates]
    );
}
