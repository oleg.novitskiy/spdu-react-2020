import React, {Component, ComponentType} from 'react';

export interface WithPaginationProps<T> {
    pageData: T[],
    data: T[]
}

interface State<T> {
    page: number,
    pageSize: number,
    pageData: T[],
    totalPages: number
}

export function withPagination<T, P extends WithPaginationProps<T> = WithPaginationProps<T>>(WrappedComponent: ComponentType<P>) {
    type Props = Readonly<Omit<P, 'pageData'>>;

    class EnhancedComponent extends Component<Props, State<T>> {
        state: State<T> = {
            pageData: [],
            page: 1,
            pageSize: 10,
            totalPages: 1
        };

        static getDerivedStateFromProps(props: Props, state: State<T>) {
            return {
                totalPages: Math.ceil(props.data.length / state.pageSize)
            }
        }

        componentDidMount() {
            this.onPageChange(1);
        }

        onPageChange = (page: number) => {
            const offsetStart = (page - 1) * this.state.pageSize;
            const offsetEnd = page * this.state.pageSize;
            const pageData = this.props.data.slice(offsetStart, offsetEnd);

            this.setState(() => ({pageData}));
        };


        render() {
            const pagination =
                new Array(this.state.totalPages).fill(null).map((_, idx) => {
                    return <button key={idx} onClick={() => this.onPageChange(idx + 1)}>{idx + 1}</button>
                });

            return (
                <div>
                    <WrappedComponent {...this.props as P} pageData={this.state.pageData}/>
                    {pagination}
                </div>
            )
        }
    }

    return EnhancedComponent;
}
