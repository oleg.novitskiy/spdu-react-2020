import React, {FC} from 'react';
import {withErrorFallback} from '../withErrorFallback';

type Props = {
    test: string
};

export const WithErrorExample: FC<Props> = () => {
    // @ts-ignore
    // console.log([].test.test);
    return (
        <>
            Everything looks good.
        </>
    )
};


export default withErrorFallback(WithErrorExample);
