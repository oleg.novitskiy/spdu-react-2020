import {useFilter} from '../hooks/use-filter';
import {students} from '../../lecture-1/students';
import {ChangeEvent, useMemo, useState} from 'react';
import {User} from '../../lecture-1/typedef';
import {Card} from '../../../components/card/card';


export const UseFilterExample = () => {
    const [inc, setInc] = useState(0);
    const [nameQuery, seNameQuery] = useState('');
    const [emailQuery, setEmailQuery] = useState('');
    const filters = useMemo(() => [
        (student: User) => student.name.toLowerCase().startsWith(nameQuery.toLowerCase()),
        (student: User) => student.email.toLowerCase().startsWith(emailQuery.toLowerCase())
    ], [nameQuery, emailQuery])
    const data = useFilter(students, filters);

    const changeNameQuery = (e: ChangeEvent<HTMLInputElement>) => {
        seNameQuery(e.target.value);
    };

    const changeEmailQuery = (e: ChangeEvent<HTMLInputElement>) => {
        setEmailQuery(e.target.value);
    };

    return (
        <div className="container">
            <button onClick={() => setInc(inc + 1)}>inc</button>
            {inc}
            <div className="col-lg-12">
                <label htmlFor="name-query">Name</label>
                <input id="name-query" type="text" value={nameQuery} onChange={changeNameQuery}/>
                <label htmlFor="email-query">Email</label>
                <input id="email-query" type="text" value={emailQuery} onChange={changeEmailQuery}/>
            </div>
            <div className="row">
                {
                    data.map(user => (
                        <div key={user.email} className="col-4 justify-content-md-center">
                            <Card user={user}/>
                        </div>
                    ))
                }
            </div>
        </div>
    )
}