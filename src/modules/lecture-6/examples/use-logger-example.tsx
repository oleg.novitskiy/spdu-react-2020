import {FC, useEffect, useState} from 'react';
import {useLogger} from '../hooks/use-logger';

interface Props {
    count: number
}

export const UseLoggerExample: FC<Props> = (props) => {
    const [count, setCount] = useState(0);

    useEffect(() => {
        const interval = setInterval(() => {
            setCount(count => count + 1);
        }, 2000)

        return () => clearInterval(interval);
    }, []);

    useLogger('UseLogger', {count});
    return <div>{count}</div>;
}