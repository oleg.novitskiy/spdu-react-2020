import {ThunkAction} from 'redux-thunk'
import {Action} from 'redux';
import {Task} from '../modules/board/typedef';

export interface AppState {
    tasks: Task[]
}

export type AppThunk<ReturnType = void> = ThunkAction<ReturnType,
    AppState,
    null,
    Action<string>>

declare module 'react-redux' {
    export interface DefaultRootState extends AppState {}
}

