import React, {FC, useState} from 'react';
import {TaskStatus} from './constants';
import {addTask} from './services/actions';
import {useDispatch} from 'react-redux';

interface Props {
    status: TaskStatus
}

export const CreateNew: FC<Props> = ({status}) => {
    const [shown, setShown] = useState(false);
    const [title, setTitle] = useState('');
    const [description, setDescription] = useState('');
    const dispatch = useDispatch();

    const save = async () => {
        await dispatch(addTask({
                title,
                description,
                status
            }
        ));
        setShown(false);
    };

    return (
        <>
            <button onClick={() => setShown(true)}>Add new</button>
            {shown
                ? <div className="card draggable shadow-sm">
                    <div className="card-body p-2">
                        <div className="card-title">
                            <i className="fas fa-check float-right" onClick={save}/>
                            <input className="form-control col-11" type="text" value={title}
                                   onChange={e => setTitle(e.currentTarget.value)}/>
                        </div>
                        <textarea className="form-control"
                                  value={description}
                                  onChange={e => setDescription(e.currentTarget.value)}
                        />
                    </div>
                </div>
                : null}
            <div> &nbsp; </div>
        </>
    );
};
