import React, {FC} from 'react';
import {Task} from './typedef';
import {BoardCard} from './board-card';
import {TaskStatus} from './constants';
import {CreateNew} from './create-new';

interface Props {
    title: string,
    tasks: Task[],
    status: TaskStatus
}

export const BoardGroup: FC<Props> = ({tasks, title, status}) => {
    return (
        <div className="col-sm-6 col-md-4 col-xl-2">
            <div className="card bg-light">
                <div className="card-body">
                    <h6 className="card-title text-uppercase text-truncate py-2">{title}</h6>
                    <div className="items border border-light">
                        {tasks && tasks.map(task => (<BoardCard task={task} key={task._id}/>))}
                        <CreateNew status={status}/>
                    </div>
                </div>
            </div>
        </div>
    );
};
