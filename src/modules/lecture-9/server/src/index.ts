import {App} from './App';

const PORT = 3005;
const app = new App();

app.express.listen(3005, (err: string): void => {
    if (err) {
        console.error('error %s', err);
    }

    console.info('Listening on port %d', PORT);
},);
