import * as express from 'express';
import * as cors from 'cors';
import * as bodyParser from 'body-parser';
import {boardRoutes as registerBoard} from './routes/board';

export class App {
    public express: express.Application;

    public constructor() {
        this.express = express();
        this.init();
    }

    private init() {
        const router = express.Router();

        this.express.use(cors());
        this.express.use(bodyParser.urlencoded({ extended: false }));
        this.express.use(bodyParser.json());
        this.express.use('/', router);

        registerBoard(router);

    }
}
