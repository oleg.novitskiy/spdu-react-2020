import * as express from "express";
import {db} from '../db';
import {Task} from '../typedef';

export const boardRoutes = (app: express.Router) => {
    app.get("/board", (req, res) => {
        db.allDocs().then(dbResponse => res.send(dbResponse.rows));
    });
    app.put("/board", (req, res) => {
        db.put(req.body)
            .then(dbResponse => res.send(dbResponse))
            .catch(err => console.log(err))
    });

    app.delete("/board", (req, res) => {
        db.remove(req.body)
            .then(dbResponse => res.send(dbResponse))
            .catch(err => console.log(err))
    });

    app.post("/board", (req, res) => {
        db.post<Omit<Task, 'id'>>(req.body).then(result => {
            res.send(result);
        }).catch(err => {
            console.log(err);
        });
    });

};
