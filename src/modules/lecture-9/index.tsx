import React from 'react';
import {Switch, Route, Redirect, useRouteMatch,} from 'react-router-dom';
import {Item as NavbarItem, Nav, Navbar, NavbarLink} from '../lecture-5/components/navbar';
import {Board} from './modules/board';
import {Provider} from 'react-redux';
import {store} from './store'

export const Lecture9Examples = () => {
    let {path} = useRouteMatch();
    return (
        <Provider store={store}>
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
                  integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
                  crossOrigin="anonymous"/>
            <Navbar>
                <>
                    <Nav>
                        <NavbarItem>
                            <NavbarLink url="/board" caption="Board"/>
                        </NavbarItem>
                        <NavbarItem>
                            <NavbarLink url="/changelog" caption="Changelog"/>
                        </NavbarItem>
                    </Nav>
                </>
            </Navbar>
            <Switch>
                <Route path={`${path}/board`} component={Board}/>
                <Route path={`${path}/changelog`} component={() => <div>changelog</div>}/>
                <Redirect to={`${path}/board`}/>
                <Route path="*">
                    404 Not found
                </Route>
            </Switch>
        </Provider>
    );
};
