import React, {FC} from 'react';
import {usePagination} from './use-pagination';

type Data = {
    caption: string,
    id: number
}

type Props = {
    data: Data[]
};


export const PageSizeUsage: FC<Props> = props => {
    const {pageData, pagination} = usePagination<Data>(props.data, 10);

    return (
        <div>
            <ul>
                {pageData && pageData.map(item => (<li key={item.id}>{item.caption}</li>))}
            </ul>
            {pagination}
        </div>
    )
};
