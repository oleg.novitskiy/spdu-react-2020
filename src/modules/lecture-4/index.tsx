import React from 'react';
import {Route, Switch, useRouteMatch} from 'react-router-dom';
import Countdown from './class-to-hooks-example';
import {CountdownHooks} from './class-to-hooks-example-modified';
import {UseCallbackExample} from './use-callback-example';
import {HeavyCalc} from './use-memo-example';
import {PageSizeUsage} from './page-size-usage';
// import {UseLoggerExample} from './use-logger';
import {useEffect, useRef, useState} from 'react';
import {ToggleExample} from './toggle';

const exampleDataSet = new Array(1000)
    .fill(null)
    .map((_, idx) => {
        return {
            id: idx,
            caption: `Item: ${idx}`
        }
    });

export const Lecture4Examples = () => {
    let {path} = useRouteMatch();
    return (
        <Switch>
            <Route path={`${path}/countdown-example`} component={() => {
                const date = +new Date('2021');
                return <Countdown endDate={date}/>
            }}/>
            <Route path={`${path}/countdown-hooks-example`} component={() => {
                const date = +new Date('2021');
                return <CountdownHooks endDate={date}/>
            }}/>
            <Route path={`${path}/use-callback-example`} component={UseCallbackExample}/>
            <Route path={`${path}/${HeavyCalc.routeName}`} component={HeavyCalc}/>
            <Route path={`${path}/use-pagination-example`} component={() => <PageSizeUsage data={exampleDataSet}/>}/>
            <Route path={`${path}/toggle-example`} component={ToggleExample}/>
        </Switch>
    )
};
