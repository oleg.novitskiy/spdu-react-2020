import React from 'react';
import {Component} from 'react';
import './countdown.css';

type Props = {
    endDate: number
}

type TimeLeft = {
    days: number,
    hours: number,
    minutes: number,
    seconds: number
};

type State = {
    timeLeft: TimeLeft
}
const REFRESH_INTERVAL = 1000;

class Countdown extends Component<Props, State> {
    state: State = {
        timeLeft: {
            days: 0,
            hours: 0,
            minutes: 0,
            seconds: 0
        }
    };
    intervalId: number = 0;

    static getRemainingTime(startDate: number) {
        const seconds = Math.floor((startDate / 1000) % 60);
        const minutes = Math.floor((startDate / 1000 / 60) % 60);
        const hours = Math.floor((startDate / (1000 * 60 * 60)) % 24);
        const days = Math.floor(startDate / (1000 * 60 * 60 * 24));

        return {days, hours, minutes, seconds};
    }

    componentDidMount() {
        const tick = () => {
            const newValue = +this.props.endDate - +new Date();
            this.setState({
                timeLeft: Countdown.getRemainingTime(newValue)
            });
        };

        this.intervalId = window.setInterval(tick, REFRESH_INTERVAL);
        tick();
    }

    componentWillUnmount() {
        clearInterval(this.intervalId);
    }

    buildElement = (caption: string, value: number) => (
        <div className="countdown__item">
            <div className="countdown__value">{value}</div>
            <div className="countdown__caption">{caption}</div>
        </div>);

    render() {
        const {days, hours, minutes, seconds} = this.state.timeLeft;
        return (
            <div className="countdown">
                <div className="countdown__image-placeholder">
                    <img src="https://media3.giphy.com/media/26ybuZsHBYL9VpU76/giphy.gif?cid=790b7611b99cf9f9b65d117caffc6899f8a1512d39214812&rid=giphy.gif"/>
                </div>
                <div className="countdown__container">
                    {this.buildElement('Days', days)}
                    {this.buildElement('Hours', hours)}
                    {this.buildElement('Minutes', minutes)}
                    {this.buildElement('Seconds', seconds)}
                </div>
            </div>
        );
    }
}

export default Countdown;
