import {useState} from 'react';

export function useShown() {
    const [shown, setShown] = useState(false);

    const toggleShown = () => {
        setShown(shown => !shown);
    }

    const show = () => {
        setShown(true);
    }
    const hide = () => {
        setShown(false);
    }


    return {shown, toggleShown, show, hide};
}