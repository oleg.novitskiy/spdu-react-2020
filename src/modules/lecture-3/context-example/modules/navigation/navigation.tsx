import React, {FC, useContext} from 'react';
import {Navbar, Item as NavbarItem, Nav, NavbarLink} from '../../components/navbar';
import {FormInline} from '../../components/form-inline';
import {UserInfo} from '../user-info';
import {AppContext, Context} from '../../app.context';
import {LangSwitcher} from '../../components/lang-switcher/lang-switcher';

type Props = {};

export const Navigation: FC<Props> = () => {
    const context = useContext<Context | null>(AppContext)

    if (!context) {
        return null;
    }

    return (
        <Navbar>
            <Nav>
                <NavbarItem>
                    <NavbarLink url="#" caption={context.dictionary.navigation.home}/>
                </NavbarItem>
                <NavbarItem>
                    <NavbarLink url="#" caption={context.dictionary.navigation.about}/>
                </NavbarItem>
                <NavbarItem>
                    <NavbarLink url="#" caption={context.dictionary.navigation.news}/>
                </NavbarItem>
                <NavbarItem>
                    <NavbarLink url="#" caption={context.dictionary.navigation.contacts}/>
                </NavbarItem>
            </Nav>
            <Nav>
                <LangSwitcher lang={context.lang} changeLang={context.changeLang}/>
            </Nav>
            <FormInline>
                <UserInfo/>
            </FormInline>
        </Navbar>
    )
};
