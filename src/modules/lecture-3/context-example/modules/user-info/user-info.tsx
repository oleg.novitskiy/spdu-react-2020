import React, {FC} from 'react';
import {Avatar} from '../../components/avatar/avatar';
import {AppContext} from '../../app.context';


export const UserInfo: FC<{}> = () => {
    return (
        <AppContext.Consumer>
            {context => {
                return context && context.user
                    ? (
                        <>
                            <Avatar size={32} rounded={true} src={context.user.avatar}/>
                            <strong>{context.user.name}</strong>
                        </>
                    )
                    : null
            }}
        </AppContext.Consumer>
    )
};
