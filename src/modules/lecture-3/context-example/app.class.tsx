import React, {Component} from 'react';
import {Dictionary, Lang} from './typedef';
import {Navigation} from './modules/navigation/navigation';
import {AppContext} from './app.context';

type Props = {};

type State = {
    lang: Lang,
    dictionary?: Dictionary
}

export class ContextExampleClass extends Component<Props, State> {
    state: State = {
        lang: 'en',
    };

    loadDictionary(lang: Lang) {
        fetch(`https://gcp-test-yq2tp6xfda-ew.a.run.app/api/v2/publicholidays/2017/AT`)
            .then(result => result.json())
            .then((data) => {
                this.setState({dictionary: data});
            });
    }

    componentDidMount() {
        this.loadDictionary(this.state.lang);
    }

    componentDidUpdate(prevProps: Readonly<Props>, prevState: Readonly<State>,) {
        if (this.state.lang !== prevState.lang) {
            this.loadDictionary(this.state.lang);
        }
    }


    render() {
        if (!this.state.dictionary) {
            return null;
        }

        const contextValue = {
            dictionary: this.state.dictionary,
            changeLang: (lang: Lang) => this.setState({lang}),
            lang: this.state.lang
        };
        return (
            <AppContext.Provider value={contextValue}>
                <div className="container">
                    <Navigation/>
                    <pre>
                </pre>
                </div>
            </AppContext.Provider>
        )
    }
}
