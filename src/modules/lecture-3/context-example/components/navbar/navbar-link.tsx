import React, {FC} from 'react';

type Props = {
    url: string,
    caption: string
};

export const NavbarLink: FC<Props> = ({url, caption}) => {
    return (
        <a className="nav-link" href={url}>{caption}</a>
    )
};
