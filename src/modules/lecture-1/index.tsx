import React from 'react';
import {Route, Switch, useRouteMatch} from 'react-router-dom';
import {UserCards} from './App';

export const Lecture1Examples = () => {
    let { path } = useRouteMatch();
    return (
        <Switch>
            <Route path={`${path}/user-cards`} component={UserCards}/>
        </Switch>
    )
};

