export type User = {
    name: string,
    email: string,
    phone: string,
    gitlab?: string,
    facebook?: string,
    linkedIn?: string,
    photoUrl: string
}