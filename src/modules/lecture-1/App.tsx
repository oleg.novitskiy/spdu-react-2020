import React from 'react';
import './App.css';
import {students} from "./students";
import {Card} from "../../components/card/card";
import {chunk} from "./utils/chunk";
import {usePagination} from '../lecture-4/use-pagination';
import {User} from './typedef';

export const UserCards = () => {
    const {pageData, pagination} = usePagination<User>(students, 4);

    return (
        <div className="app-container">
            {chunk(pageData, 3).map((rowData, idx) => (
                <div className="row" key={idx}>
                    {rowData.map(student => <Card user={student} key={student.name}/>)}
                </div>
            ))}
            {pagination}
        </div>
    );
}

