import React from 'react';
import './avatar.scss';

interface Props {
    imgUrl: string,
    size: Sizes,
    rounded?: boolean
}

export enum Sizes {
    S,
    M,
    L
}

const SIZE_CLASSESS = {
    [Sizes.S]: 'size-s',
    [Sizes.M]: 'size-m',
    [Sizes.L]: 'size-l'
}

export const Avatar = ({imgUrl, size, rounded}: Props) => {
    return (
        <div className={`avatar avatar_${SIZE_CLASSESS[size]} ${rounded ? 'avatar_rounded': ''}`}>
            <img className="avatar__image" src={imgUrl} alt="avatar"/>
        </div>
    )
};
