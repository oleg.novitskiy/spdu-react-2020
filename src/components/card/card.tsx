import React from "react";
import {User} from "../../modules/lecture-1/typedef";
import './card.scss';
import {useState} from "react";
import {Avatar, Sizes} from "../avatar/avatar";

interface Props {
    user: User
}

export const Card = ({user}: Props) => {
    const [phoneShown, setPhoneShown] = useState(false);
    const togglePhoneVisibility = () => {
        setPhoneShown(!phoneShown);
    }

    return (
        <div className="card">
        <div className="card__header">
            <div className="card__profile-img">
                <Avatar imgUrl={user.photoUrl} size={Sizes.L} rounded={true}/>
            </div>
        </div>
        <div className="card__body">
            <p className="card__full-name">{user.name}</p>
            <p className="card__email">{user.email}</p>
            <p className="card__phone" onClick={togglePhoneVisibility}><i className="fas fa-phone"/>{phoneShown ? user.phone : '...'}</p>
            <p className="card__links">
                {user.facebook &&
                <a href={user.facebook} className="card__social-icon card__social-icon_facebook"><i className="fab fa-facebook-f"/></a>}
                {user.gitlab &&
                <a href={user.gitlab} className="card__social-icon card__social-icon_gitlab"><i className="fab fa-gitlab"/></a>}
                {user.linkedIn &&
                <a href={user.linkedIn} className="card__social-icon card__social-icon_linkedin"><i className="fab fa-linkedin"/></a>}
            </p>
        </div>
    </div>
)
};
